# Winezeug Archaeology (v2)

Welcome back to the winezeug dig-site.

If you have questions or comments, feel free to contact the excavation
supervisor:

Kyle Auble <kyle.auble@zoho.com>


# What's Up with Version 2?

While dipping my toes back into this project after a long hiatus, I
found out that there were still some occasional changes happening to
winezeug. However, when I tried pulling these changes, it turned out
that the active repo had entirely different hashes from Dan Kegel's
repo.

Since I don't think anyone's working from my repo, I decided to rebase
mine on the active one. I also took the opportunity to squash a couple
"oops" commits and bring my excavate and master branches into line.
For propriety's sake though, I've uploaded this as a separate repo
from v1, which I'll keep online for at least a year.

But from here on out, all work will be happening here.


# About

The goal of this repository is to sort through Dan Kegel's old
winezeug repo. Anything old or unnecessary will get tossed, but there
are also some interesting ideas certainly worth preserving. I hope to
polish those up and get them merged into the appropriate repos
(winetricks, WineHQ tools, etc.)

If you want to help, I would definitely appreciate patches or advice;
just don't expect them to be committed right away. If you're on
Bitbucket, you can just send me a pull request, and though I can't get
pull requests from Github, if you plan on making regular changes, let
me know and I can still track your fork with a remote branch. Or you
can always email me a "git diff" patch.


# Procedure

If you read the Git history for the 'excavate' branch, you'll see I'm
using hefty commit messages. You can be much shorter and tweak the
format if you want. I just ask that you keep your subject line under
50 chars, the rest under 72, and include a short explanation for any
files you delete or move.

Thanks for the help!
